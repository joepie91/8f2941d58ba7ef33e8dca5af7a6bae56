const ChartjsNode = require('chartjs-node');
// 600x600 canvas size
var chartNode = new ChartjsNode(600, 600);
var randomnumber=Math.random();
var imagename = "testimage"+randomnumber+".png"
module.exports = imagename

// each api returns a Promise
chartNode.drawChart({
        type: 'bar',
        data: {
            labels: ["2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"],
            datasets: [{
                label: 'Antal akkrediteringer',
                data: [57, 125, 249, 262, 271, 289, 227, 98, 126, 93, 41],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            layout: {
                padding:{
                    left: 30,
                    right: 30, 
                    top: 30,
                    bottom: 30
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    })
    .then(() => {
        // now we have a chart
        // lets get the image stream
        return chartNode.getImageStream('image/png');
    })
    .then(imageStream => {
        // now you can do anything with the image, like upload to S3
        // lets get the image buffer
        return chartNode.getImageBuffer('image/png');
    })
    .then(imageBuffer => {
        // now you can modify the raw PNG buffer if you'd like
        // want to write the image directly to the disk, no problem
        return chartNode.writeImageToFile('image/png', '/home/nicki/akkredibot/img/'+imagename);
    })
    .then(() => {
       chartNode.destroy() 
    });